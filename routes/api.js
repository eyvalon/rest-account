const express = require("express");
const users = require("./users");

const router = express.Router();

/* GET api listing. */
router.get("/", (req, res) => {
  res.send("api works");
});

// API calls go here
router.use("/users", users);

module.exports = router;
