# Building a RESTful API with NodeJs, Express, MongoDB and JWT

## Description

- This is a server implemented to build a RESTful API, allowing users to call for GET / POST endpoints for securely logging in.

## Installation

- Install dependencies: `npm install`
- Start the server: `node server.js`
