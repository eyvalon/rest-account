// Dependencies
const express = require("express");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const config = require("./config/database");
var bodyParser = require("body-parser");

// Express and Port
const app = express();
const port = process.env.PORT || 3000;

// Connect to database via mongoose
mongoose
  .connect(config.database, { useNewUrlParser: true })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log(err));

// Routes
const api = require("./routes/api");

// ******************************************
// MIDDLEWARE
// ******************************************
// Cross-Origin Resource Sharing (CORS)

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200
};

app.use(cors(corsOptions));

app.get("/", (req, res) => {
  res.send("Are you sure you are a human?");
});

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Passport
require("./config/passport")(passport);
app.use(passport.initialize());
app.use(passport.session());

// API calls for serving external calls
app.use("/api", api);

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// START THE SERVER
// =============================================================================
app.set("port", port);

app.listen(port, () => console.log(`Magic happens on port ${port}`));
